This tutorial shows you how to create a new custom field type for JIRA using the plugin development platform. 
You'll create a custom field that can only be edited as an admin user. 
Your custom field will be written and installed as a JIRA plugin, and include a Java class that extends the 
GenericTextCFT class to enable storage and retrieval of your custom field values. 
You'll use a Velocity template, a format that combines Java and HTML, to render your field and control who 
can edit or view the field. To tether your Java class and template together, you'll define both in a single 
customfield-type plugin module in your atlassian-plugin.xml descriptor file. 

This tutorial covers the following topics: 

* An overview of custom fields and the files that comprise the plugin
* Extending the custom field type class, GenericTextCFT, for your plugin
* Using the customfield-type plugin module type for JIRA

You can view the full tutorial here: 

https://developer.atlassian.com/display/JIRADEV/Creating+a+Custom+Field+Type
